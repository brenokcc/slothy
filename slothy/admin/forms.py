# -*- coding: utf-8 -*-
from django.conf import settings
from django import forms
from slothy.api.auth import authenticate
from slothy.forms import Form


class LoginForm(Form):
    username = forms.CharField(label='Login')
    password = forms.CharField(label='Senha')

    class Meta:
        title = 'Acesso ao Sistema'
        image = settings.PROJECT_LOGO
        center = True
        lookups = ()
        fieldsets = {
            None: ('username', 'password')
        }

    def show(self):
        return super().show()

    def submit(self):
        username = self.data['username']
        password = self.data['password']
        return authenticate(self.request, username, password)
