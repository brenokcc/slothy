# -*- coding: utf-8 -*-

from django.contrib import auth
from django.core import signing
from django.core.exceptions import ValidationError


def authenticate(request, username, password):
    user = auth.authenticate(
        request, username=username,
        password=password
    )
    if user:
        auth.login(request, user)
        return dict(
            type='login',
            message='Login realizado com sucesso',
            token=signing.dumps(request.user.id)
        )
    raise ValidationError('Usuário e senha não conferem')
