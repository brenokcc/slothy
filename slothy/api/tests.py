# -*- coding: utf-8 -*-

from django.test import TestCase
import json

from slothy.admin.models import User


class ApiTestCase(TestCase):

    def setUp(self):
        super().setUp()
        User.objects.create_superuser()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.log = False
        self.token = None

    def print(self, response):
        if self.log:
            print(json.dumps(response, indent=2, sort_keys=False, ensure_ascii=False))

    def debug(self, log=True):
        self.log = log

    def header(self):
        if self.token:
            return {'Authorization': 'Token '.format(self.token)}
        return None

    def login(self, username, password):
        data = dict(username=username, password=password)
        response = self.post('/api/login', data=data)
        self.token = response['token']

    def logout(self):
        self.token = None

    def user(self):
        return self.get('/api/user')

    def assertDisplays(self, queryset_response, attr_name):
        names = [field['name'] for field in queryset_response['metadata']['display']]
        return self.assertIn(attr_name, names)

    def assertNotDisplays(self, queryset_response, attr_name):
        names = [field['name'] for field in queryset_response['metadata']['display']]
        return self.assertNotIn(attr_name, names)

    def get(self, url, data=None):
        data = json.dumps(data) if data is not None else None
        response = self.client.get(url, data=data, content_type='application/json', header=self.header())
        response = json.loads(response.content)
        self.print(response)
        return response

    def post(self, url, data=None):
        data = json.dumps(data) if data is not None else None
        response = self.client.post(url, data=data, content_type='application/json', header=self.header())
        response = json.loads(response.content)
        self.print(response)
        return response
