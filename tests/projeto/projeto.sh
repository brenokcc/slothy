#!/bin/bash
set -e
if [ ! -d ".virtualenv" ]; then
 python3.8 -m pip install virtualenv
 python3.8 -m virtualenv .virtualenv
 source .virtualenv/bin/activate
 python3.8 -m pip install -r requirements.txt
else
 source .virtualenv/bin/activate
fi

mkdir -p logs
python3.8 manage.py sync
echo "Starting gunicorn..."
gunicorn projeto.wsgi:application -w 1 -b 127.0.0.1:${1:-8000} --timeout=600 --user=${2:-$(whoami)} --log-level=_debug --log-file=logs/gunicorn.log 2>>logs/gunicorn.log
