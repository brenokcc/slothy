# Generated by Django 3.1.7 on 2021-03-14 17:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import slothy.db.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('admin', '0001_initial'),
        ('enderecos', '0002_auto_20210129_0606'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cidade',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', slothy.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('localizacao', slothy.db.models.fields.GeoLocationField(blank=True, max_length=255, null=True, verbose_name='Localização')),
            ],
            options={
                'verbose_name': 'Cidade',
                'verbose_name_plural': 'Cidades',
            },
        ),
        migrations.CreateModel(
            name='Endereco',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logradouro', slothy.db.models.fields.CharField(max_length=100, verbose_name='Logradouro')),
                ('numero', models.IntegerField(verbose_name='Número')),
                ('cidade', slothy.db.models.fields.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='projeto.cidade', verbose_name='Cidade')),
            ],
            options={
                'verbose_name': 'Endereço',
                'verbose_name_plural': 'Endereços',
            },
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', slothy.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('sigla', slothy.db.models.fields.CharField(max_length=255, verbose_name='Sigla')),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo')),
                ('cor', slothy.db.models.fields.ColorField(blank=True, default='#FFFFFF', max_length=10, verbose_name='Cor')),
            ],
            options={
                'verbose_name': 'Estado',
                'verbose_name_plural': 'Estados',
            },
        ),
        migrations.CreateModel(
            name='Pessoa',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('nome', slothy.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('email', slothy.db.models.fields.EmailField(max_length=255, unique=True, verbose_name='E-mail')),
                ('foto', models.ImageField(blank=True, null=True, upload_to='fotos', verbose_name='Foto')),
                ('endereco', slothy.db.models.fields.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projeto.endereco', verbose_name='Endereço')),
            ],
            options={
                'verbose_name': 'Pessoa',
                'verbose_name_plural': 'Pessoas',
            },
            bases=('admin.user',),
        ),
        migrations.CreateModel(
            name='PontoTuristico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', models.ImageField(blank=True, null=True, upload_to='fotos', verbose_name='Foto')),
                ('nome', slothy.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo')),
            ],
            options={
                'verbose_name': 'Ponto Turístico',
                'verbose_name_plural': 'Pontos Turísticos',
            },
        ),
        migrations.CreateModel(
            name='Telefone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ddd', models.IntegerField(verbose_name='DDD')),
                ('numero', slothy.db.models.fields.CharField(max_length=255, verbose_name='Telefone')),
            ],
            options={
                'verbose_name': 'Telefone',
                'verbose_name_plural': 'Telefones',
            },
        ),
        migrations.CreateModel(
            name='Municipio',
            fields=[
            ],
            options={
                'verbose_name': 'Município',
                'verbose_name_plural': 'Municípios',
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('enderecos.municipio',),
        ),
        migrations.CreateModel(
            name='Presidente',
            fields=[
                ('pessoa_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='projeto.pessoa')),
            ],
            options={
                'verbose_name': 'Presidente',
                'verbose_name_plural': 'Presidentes',
            },
            bases=('projeto.pessoa',),
        ),
        migrations.AddField(
            model_name='pessoa',
            name='telefones',
            field=slothy.db.models.fields.OneToManyField(to='projeto.Telefone', verbose_name='Telefones'),
        ),
        migrations.CreateModel(
            name='Governador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', slothy.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projeto.estado', verbose_name='Estado')),
                ('pessoa', slothy.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projeto.pessoa', verbose_name='Pessoa')),
            ],
            options={
                'verbose_name': 'Governador',
                'verbose_name_plural': 'Governadores',
            },
        ),
        migrations.AddField(
            model_name='cidade',
            name='estado',
            field=slothy.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projeto.estado', verbose_name='Estado'),
        ),
        migrations.AddField(
            model_name='cidade',
            name='pontos_turisticos',
            field=slothy.db.models.fields.ManyToManyField(blank=True, to='projeto.PontoTuristico', verbose_name='Pontos Turísticos'),
        ),
        migrations.AddField(
            model_name='cidade',
            name='prefeito',
            field=slothy.db.models.fields.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='projeto.pessoa', verbose_name='Prefeito'),
        ),
        migrations.AddField(
            model_name='cidade',
            name='vereadores',
            field=slothy.db.models.fields.ManyToManyField(blank=True, related_name='cidades_legisladas', to='projeto.Pessoa', verbose_name='Vereadores'),
        ),
    ]
