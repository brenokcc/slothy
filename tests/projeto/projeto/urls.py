# -*- coding: utf-8 -*-

from slothy.admin import views
from django.conf.urls import url
from slothy.api.urls import urlpatterns as api_urls
from slothy.admin.urls import urlpatterns as admin_urls

urlpatterns = [
    url(r"^$", views.index),
    url(r"^static/app/$", views.index),
] + api_urls + admin_urls
