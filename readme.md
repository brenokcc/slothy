# Slothy

![Kiku](slogan.png)


## Installation

### Python

> pip install slothy


## Creating a Project


>python -m slothy startproject < project-name >
>
> cd < project-name >
>
> python manage.py sync
>
>python manage.py runserver
